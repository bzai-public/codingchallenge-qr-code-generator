import lib.qrdata.{ErrorCorrectionLevel, QR}
import lib.qrdata.QRBitStringFuncs.*
import lib.errorCorrection.*
import lib.qrimage.*

object main extends App {
    
    // Replace s with the string you want to encode.
    val s = "https://gitlab.com/bzai-public/codingchallenge-qr-code-generator/"
    
    println("Generating QR Code:")
    println(s"\tData String: $s")
    
    println("\tGetting required QR code parameters...")
    val dat = QR(s)
    val width = QRArray.width
    
    println("\tEncoding data string to bits...")
    val dataBitString = getDataBitString(dat)
    val l = getErrorCorrectionLevel(dat).get
    val allCodeWords = getFullDataCodeWords(dat)

    println("\tGenerating error correction codes...")
    val bitStringWithEC = generateBinaryStringWithErrorCorrection(allCodeWords,
        l.generatorPolyExponents,
        l.nCodeWords / l.nBlocks,
        l.nBlocks)
    
    println("\tGenerating QR image...")
    println("\t\t- Placing a required bits into image...")
    val initialQR = QRArray.placeAllBits(bitStringWithEC)
    
    println("\t\t- Applying and selecting best QR mask to use...")
    val (maskID, bestMaskedQR) = QRMask.selectBestMask(initialQR)
    
    println("\t\t- Calculating information and version bits...")
    val infobits = QRInfo.calculateInfoBits(l, maskID)
    
    println("\t\t- Placing information and version bits and inserting quiet area around QR...")
    val finalQR = QRArray.placeQuietArea(QRInfo.placeInfoBits(bestMaskedQR, infobits))
    
    println("\t\t- Rendering image...")
    Renderer.generate(finalQR)
    
    println("#### DONE! ####")

}
