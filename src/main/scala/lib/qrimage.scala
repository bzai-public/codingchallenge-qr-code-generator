package lib.qrimage {
    
    import lib.qrdata.ErrorCorrectionLevel
    import lib.qrimage.QRModule.{Black, White}
    import lib.utils.QRHelpers.leftPadZeroBinaryString

    import java.awt.Image
    import java.awt.image.BufferedImage
    import java.io.File
    import javax.imageio.ImageIO
    
    trait Colour
    trait WhiteColour extends Colour
    trait BlackColour extends Colour
    
    enum QRModule extends Colour{
        // Data and error correction bits
        case White extends QRModule with WhiteColour
        case Black extends QRModule with BlackColour
        // Finder bits
        case FBlack extends QRModule with BlackColour
        case FWhite extends QRModule with WhiteColour
        // Separator bits
        case SBlack extends QRModule with BlackColour
        case SWhite extends QRModule with WhiteColour
        // Alignment bits
        case ABlack extends QRModule with BlackColour
        case AWhite extends QRModule with WhiteColour
        // Timing Bits
        // (V suffix for vertical timing pattern. This is needed when placing data bits)
        case TBlack extends QRModule with BlackColour
        case TWhite extends QRModule with WhiteColour
        case TBlackV extends QRModule with BlackColour
        case TWhiteV extends QRModule with WhiteColour
        // Info bits
        case Info extends QRModule with WhiteColour
    }
    
    object QRArray {
        
        import QRModule._
        
        val version = 4
        val width = ((version - 1) * 4) + 21
        
        val qrArr = Array.ofDim[QRModule](width, width)
        
        type QRArrType = Array[Array[QRModule]]
        
        def printArr(arr: QRArrType): Unit = {
            println(arr.map(_.mkString(",")).mkString("\n"))
        }
        
        def transposeArray(qrArr: QRArrType): QRArrType = {
            val arrWidth = qrArr.length
            val newArr = Array.ofDim[QRModule](arrWidth, arrWidth)
            
            for (i <- (0 to (arrWidth - 1));
                j <- (0 to (arrWidth - 1))) {
                newArr(j)(i) = qrArr(i)(j)
            }
            
            newArr
        }
        
        val finderPattern = List(
            (1 to 7).map(_ => FBlack).toList,
            List(FBlack) ++ (1 to 5).map(_ => FWhite) ++ List(FBlack),
            List(FBlack, FWhite) ++ (1 to 3).map(_ => FBlack) ++ List(FWhite, FBlack),
            List(FBlack, FWhite) ++ (1 to 3).map(_ => FBlack) ++ List(FWhite, FBlack),
            List(FBlack, FWhite) ++ (1 to 3).map(_ => FBlack) ++ List(FWhite, FBlack),
            List(FBlack) ++ (1 to 5).map(_ => FWhite) ++ List(FBlack),
            (1 to 7).map(_ => FBlack).toList
        )
        
        val topLeftSeparatorCoords = (0 to 7).map(i => (i, 7)).toList ++ (0 to 6).map(i => (7, i)).toList
        
        def placeFinderPattern(startRow: Int, startCol: Int, qrArr: QRArrType): QRArrType = {
            for row <- (0 to 6)
                col <- (0 to 6)
            yield qrArr(startRow + row)(startCol + col) = finderPattern(row)(col)
            
            qrArr
        }
        
        def placeAllFinderPatterns(qrArr: QRArrType): QRArrType = {
            placeFinderPattern(width - 7, 0, placeFinderPattern(0, 0, placeFinderPattern(0, width - 7, qrArr)))
        }
        
        def placeSeparatorPatterns(qrArr: QRArrType): QRArrType = {
            // place top left
            for (row, col) <- topLeftSeparatorCoords yield qrArr(row)(col) = SWhite
            
            // place top right
            val topRightSeparatorCoords = topLeftSeparatorCoords.map((i, j) => (i, width - j - 1))
            for (row, col) <- topRightSeparatorCoords yield qrArr(row)(col) = SWhite
            
            // place bottom left
            val bottomLeftSeparatorCoords = topLeftSeparatorCoords.map((i, j) => (width - i - 1, j))
            for (row, col) <- bottomLeftSeparatorCoords yield qrArr(row)(col) = SWhite
            
            qrArr
        }
        
        val alignmentPattern = List(
            (1 to 5).map(_ => ABlack).toList,
            List(ABlack) ++ (1 to 3).map(_ => AWhite) ++ List(ABlack),
            List(ABlack, AWhite, ABlack, AWhite, ABlack),
            List(ABlack) ++ (1 to 3).map(_ => AWhite) ++ List(ABlack),
            (1 to 5).map(_ => ABlack).toList
        )
        
        def placeAlignmentPattern(qrArr: QRArrType): QRArrType = {
            // for version 4 there is only 1 alignment pattern with its center at (26, 26)
            // this means that the top left corner of the alignment pattern is at (24, 24)
            
            for row <- (0 to 4)
                col <- (0 to 4)
            yield qrArr(24 + row)(24 + col) =alignmentPattern(row)(col)
            
            qrArr
        }
        
        def placeTimingPatterns(qrArr: QRArrType): QRArrType = {
            for col <- (8 to (width - 8 - 1)) yield qrArr(6)(col) = if ((col - 8) % 2) == 0 then TBlack else TWhite
            
            for row <- (8 to (width - 8 - 1)) yield qrArr(row)(6) = if ((row - 8) % 2) == 0 then TBlackV else TWhiteV
            
            qrArr
        }
        
        def placeDarkModule(qrArr: QRArrType): QRArrType = {
            qrArr((4 * version) + 9)(8) = Black
            
            qrArr
        }
        
        def placeInfoPatterns(qrArr: QRArrType): QRArrType = {
            
            for row <- (0 to 8) yield if (qrArr(row)(8) == null) then qrArr(row)(8) = Info
            
            for col <- (0 to 8) yield if (qrArr(8)(col) == null) then qrArr(8)(col) = Info
            
            for col <- ((width - 8) to (width - 1)) yield if (qrArr(8)(col) == null) then qrArr(8)(col) = Info
            
            for row <- ((width - 8) to (width - 1)) yield if (qrArr(row)(8) == null) then qrArr(row)(8) = Info
            
            qrArr
        }
        
        enum PlacementDirection {
            case Up
            case Down
        }
        
        enum PlacementLane {
            case Right
            case Left
        }
        
        case class Position(curRow: Int, curCol: Int, direction: PlacementDirection, lane: PlacementLane) {
            def next: Either[String, Position] = {
                direction match {
                    case PlacementDirection.Up => {
                        lane match {
                            case PlacementLane.Right => {
                                val nextRow = curRow
                                val nextCol = curCol - 1
                                
                                if nextCol < 0 then {
//                                    Left(s"Out of bounds! Current cell is on a row index < 0! CurRow: $curRow, CurCol: $curCol, Dir: ${this.direction}, Lane: ${this.lane}")
                                    val nextRow = curRow - 1
                                    val nextCol = curCol

                                    Right(Position(nextRow, nextCol, this.direction, PlacementLane.Right))
                                }
                                else Right(Position(nextRow, nextCol, this.direction, PlacementLane.Left))
                            }
                            case PlacementLane.Left => {
                                val nextRow = curRow - 1
                                val nextCol = curCol + 1
                                
                                if nextRow < 0 then {
                                    val nextRow = curRow
                                    val nextCol = curCol - 1
                                    
                                    Right(Position(nextRow, nextCol, PlacementDirection.Down, PlacementLane.Right))
                                } else {
                                    Right(Position(nextRow, nextCol, this.direction, PlacementLane.Right))
                                }
                                
                            }
                        }
                    }
                    case PlacementDirection.Down => {
                        lane match {
                            case PlacementLane.Right => {
                                val nextRow = curRow
                                val nextCol = curCol - 1
                                
                                if nextCol < 0 then {
                                    Left(s"Out of bounds! Current cell is on a row index < 0! CurRow: $curRow, CurCol: $curCol, Dir: ${this.direction}, Lane: ${this.lane}")
                                }
                                else Right(Position(nextRow, nextCol, this.direction, PlacementLane.Left))
                            }
                            
                            case PlacementLane.Left => {
                                val nextRow = curRow + 1
                                val nextCol = curCol + 1
                                
                                if nextRow >= width then {
                                    
                                    val nextRow = curRow
                                    val nextCol = curCol - 1
                                    
                                    if nextCol < 0 then Left("Out of bounds! Current cell is on a row index < 0!")
                                    else {
                                        Right(Position(nextRow, nextCol, PlacementDirection.Up, PlacementLane.Right))
                                    }
                                } else {
                                    Right(Position(nextRow, nextCol, this.direction, PlacementLane.Right))
                                }
                            }
                        }
                    }
                }
            }
            
            def nextValid(qrArr: QRArrType): Either[String, Position] = {
                
                val nextPos = this.next
                
                nextPos match {
                    case Left(s) => nextPos
                    case Right(p) => {
                        if ((p.direction == PlacementDirection.Down) & (qrArr(p.curRow)(p.curCol) == TBlackV)) then {
                            // this is for the special case when the data bits hit the vertical alignment patterns
                            val newPos = Position(curRow + 2, curCol, PlacementDirection.Down, PlacementLane.Right)
                            Right(newPos)
                        } else {
                            if (qrArr(p.curRow)(p.curCol) == null) then {
                                nextPos
                            } else {
                                p.nextValid(qrArr)
                            }
                        }
                    }
                }
            }
        }
        
        def placeDataBits(qrArr: QRArrType, message: String): QRArrType = {
            
            var pos = Position(width - 1, width - 1, PlacementDirection.Up, PlacementLane.Right)
            val qrMsg = message.map({
                case '1' => QRModule.Black
                case '0' => QRModule.White
            }).toList
            
            def go(l: List[QRModule]): QRArrType = {
                
                l match {
                    
                    case h :: t => {
                        
                        qrArr(pos.curRow)(pos.curCol) = h
                        
                        if t.nonEmpty then
                            
                            pos.nextValid(qrArr) match {
                                case Right(p) => pos = p
                                case Left(s) => {
                                    println(s)
                                    println(t)
                                    qrArr
                                }
                            }
                        
                            go(t)
                        
                        else
                            qrArr
                        
                    }
                    
                    case _ => ???
                    
                    }
                }
            
            go(qrMsg)
            
        }
        
        def placeAllBits(bitStringWithEC: String) = {
            val x = QRArray.placeInfoPatterns(
                QRArray.placeDarkModule(
                    QRArray.placeTimingPatterns(
                        QRArray.placeAlignmentPattern(
                            QRArray.placeSeparatorPatterns(
                                QRArray.placeAllFinderPatterns(QRArray.qrArr)
                            )
                        )
                    )
                )
            )
            
            QRArray.placeDataBits(x, bitStringWithEC)
        }
        
        def placeQuietArea(qrArr: QRArrType): QRArrType = {
            val arrWidth = qrArr.length
            
            val newArrWidth = arrWidth + 8
            
            val newArr = Array.ofDim[QRModule](newArrWidth, newArrWidth)
            
            for (i <- (0 until newArrWidth);
                 j <- (0 until newArrWidth)) {
                if (i < 4) || (i >= 37) || (j < 4) || (j >= 37) then {
                    newArr(i)(j) = QRModule.White
                } else {
                    newArr(i)(j) = qrArr(i - 4)(j - 4)
                }
            }
            
            newArr
        }

    }
    
    object QRMask {
        
        import QRArray.*
        import QRModule.*
        
        def flipBit(bit: QRModule): QRModule = {
            bit match {
                case Black => White
                case White => Black
                case _ => bit
            }
        }
        
        def maskWithRowColIndexCond(qrArr: QRArrType)(f: (Int, Int) => Boolean): QRArrType = {
            qrArr.zipWithIndex
                .map((row, rowIdx) => {
                    row.zipWithIndex
                        .map((col, colIdx) => {
                            if f(rowIdx, colIdx) then {
                                flipBit(col)
                            } else {
                                col
                            }
                        })
                })
        }
        
        def applyMask0(qrArr: QRArrType): QRArrType = {
            maskWithRowColIndexCond(qrArr)((r, c) => (r + c) % 2 == 0)
        }

        def applyMask1(qrArr: QRArrType): QRArrType = {

            qrArr.zipWithIndex
                .map((row, rowIdx) => {
                    if (rowIdx % 2 == 0) then {
                        row.map(col => flipBit(col))
                    } else {
                        row
                    }
                })
        }
        
        def applyMask2(qrArr: QRArrType): QRArrType = {
            
            qrArr.map(row => {
                row.zipWithIndex
                    .map((col, colIdx) => {
                        if (colIdx % 3 == 0) then {
                            flipBit(col)
                        } else {
                            col
                        }
                    })
            })
        }

        def applyMask3(qrArr: QRArrType): QRArrType = {
            maskWithRowColIndexCond(qrArr)((r, c) => (r + c) % 3 == 0)
        }
        
        def applyMask4(qrArr: QRArrType): QRArrType = {
            maskWithRowColIndexCond(qrArr)((r, c) => (math.floor(r / 2) + math.floor(c/3)) % 2 == 0)
        }
        
        def applyMask5(qrArr: QRArrType): QRArrType = {
            maskWithRowColIndexCond(qrArr)((r, c) => {
                ((r * c) % 2) + ((r  * c) % 3) == 0
            })
        }
        
        def applyMask6(qrArr:QRArrType): QRArrType = {
            maskWithRowColIndexCond(qrArr)((r, c) => {
                (((r * c) % 2) + ((r * c) % 3)) % 2 == 0
            })
        }
        
        def applyMask7(qrArr: QRArrType): QRArrType = {
            maskWithRowColIndexCond(qrArr)((r, c) => {
                (((r + c) % 2) + ((r + c) % 3)) % 2 == 0
            })
        }
        
        def applyAllMasks(qrArr: QRArrType): List[QRArrType] = {
            List(
                applyMask0(qrArr),
                applyMask1(qrArr),
                applyMask2(qrArr),
                applyMask3(qrArr),
                applyMask4(qrArr),
                applyMask5(qrArr),
                applyMask6(qrArr),
                applyMask7(qrArr),
            )
        }
        
        object MaskPenalties {
            def darkToLightRatio(qrArr: QRArrType): Int = {
                val arrWidth = qrArr.length
                
                val nModules = arrWidth * arrWidth
                
                val nDarkModules = qrArr.map(row => {
                    row.count(elem => (elem == Black) ||
                        (elem == FBlack) ||
                        (elem == SBlack) ||
                        (elem == ABlack) ||
                        (elem == TBlack))
                }).reduce(_ + _)
                
                val percentDark = nDarkModules.toFloat / nModules.toFloat
                
                val lower = math.abs((math.floor(percentDark * 20) / 20 * 100) - 50).toInt / 5
                val upper = math.abs((math.ceil(percentDark * 20) / 20 * 100) - 50).toInt / 5

                (math.min(lower, upper) * 10).toInt
            }
            
            def matchBitColor[A<:Colour](bit1: A, bit2: A): Boolean = {
                bit1 match {
                    case _: BlackColour => bit2 match {
                        case _: BlackColour => true
                        case _: WhiteColour => false
                    }
                    case _: WhiteColour => bit2 match {
                        case _: BlackColour => false
                        case _: WhiteColour => true
                    }
                }
            }
            
            def consecutiveColors(qrArr: QRArrType): Int = {
                
                def searchRow(winLength:Int, score: Int, sameColour: Boolean, lastBitColour: QRModule, row: List[QRModule]): Int = {
                    
                    winLength match {
                        case 0 => {
                            row match {
                                case h::t => searchRow(1, score, true, h, t)
                                case _ => score
                            }
                        }
                        case l if l < 5 => {
                            row match {
                                case h::t => {
                                    if matchBitColor[QRModule](lastBitColour, h) then
                                        searchRow(l + 1, score, true, h, t)
                                    else
                                        searchRow(0, score, true, h, t)
                                }
                                case _ => score
                            }
                        }
                        case l if l == 5 => {
                            row match {
                                case h::t => {
                                    if matchBitColor[QRModule](lastBitColour, h) then
                                        searchRow(l + 1, score + 3, true, h, t)
                                    else
                                        searchRow(0, score, false, lastBitColour, row)
                                }
                                case _ => score
                            }
                        }
                        case l if l > 5 => {
                            row match {
                                case h :: t => {
                                    if matchBitColor[QRModule](lastBitColour, h) then
                                        searchRow(l + 1, score + 1, true, h, t)
                                    else
                                        searchRow(0, score, false, lastBitColour, row)
                                }
                                case _ => score
                            }
                        }
                    }
                }
                
                qrArr.map(row => searchRow(0, 0, false, QRModule.Black, row.toList)).reduce(_ + _) +
                    transposeArray(qrArr).map(row => searchRow(0, 0, false, QRModule.Black, row.toList)).reduce(_ + _)
            }
            
            def blocksSameColor(qrArr: QRArrType): Int = {
                val arrWidth = qrArr.length
                
                var score = 0
                
                for (curRow <- (0 to (arrWidth - 2));
                     curCol <- (0 to (arrWidth - 2))) {
                    
                    if (matchBitColor(qrArr(curRow)(curCol), qrArr(curRow)(curCol)) &&
                        matchBitColor(qrArr(curRow)(curCol), qrArr(curRow)(curCol + 1)) &&
                        matchBitColor(qrArr(curRow)(curCol), qrArr(curRow + 1)(curCol))) then {
                        score += 3
                    }
                }
                
                score
            }
            
            def darkLightSequence(qrArr: QRArrType): Int = {
                
                val left = Array[QRModule](Black, White, Black, Black, Black, White, Black, White, White, White, White)
                val right = Array[QRModule](White, White, White, White, Black, White, Black, Black, Black, White, Black)
                
                def matchSeq(arr1: Array[QRModule], arr2: Array[QRModule]): Boolean = {
                    for ((b1, b2) <- arr1.zip(arr2)) {
                        if !matchBitColor(b1, b2) then return false
                    }
                    true
                }
                
                def matchAllRows(qrArr: QRArrType, ref: Array[QRModule]): Int = {
                    qrArr.map(row => {
                        row.sliding(11, 11)
                            .map(win => matchSeq(win, left))
                            .count(_ == true)
                    }).reduce(_ + _) * 40
                }
                
                val qrArrTranspose = transposeArray(qrArr)
                
                matchAllRows(qrArr, left) + matchAllRows(qrArr, right) +
                    matchAllRows(qrArrTranspose, left) + matchAllRows(qrArrTranspose, right)
            }
            
            def applyAll(qrArr: QRArrType): Int = {
                darkToLightRatio(qrArr) +
                    consecutiveColors(qrArr) +
                    blocksSameColor(qrArr) +
                    darkLightSequence(qrArr)
            }
        }
        
        def applyMasksAndCalculatePenalties(qrArr: QRArrType): List[(Int, QRArrType, Int)] = {
            applyAllMasks(qrArr).zipWithIndex.map((arr, idx) => (idx, arr, MaskPenalties.applyAll(arr)))
        }
        
        def selectBestMask(qrArr: QRArrType): (Int, QRArrType) = {
            applyMasksAndCalculatePenalties(qrArr)
                .reduce((a, b) => {
                    
                    val (idx1, arr1, p1) = a
                    val (idx2, arr2, p2) = b
                    
                    if p1 <= p2 then
                        (idx1, arr1, p1)
                    else
                        (idx2, arr2, p2)
                }) match {
                case (id, arr, p) => (id, arr)
            }
        }
        
    }
    
    object QRInfo {
        
        import QRArray.*
        import QRModule.*
        
        def get5BitFormatString(l: ErrorCorrectionLevel, maskID: Int) = {
            l.infoIDString + leftPadZeroBinaryString(maskID.toBinaryString, 3)
        }
        
        def calculateInfoBits(l: ErrorCorrectionLevel, maskId: Int) = {
            
            val genString = "10100110111"
            val infoStringRaw = get5BitFormatString(l, maskId)
            val infoString = Integer.parseInt(infoStringRaw.padTo(15, '0'), 2).toBinaryString
            
            val nRounds = infoString.length - 10
            
            def xor(s1: String, s2: String): String = {
                (Integer.parseInt(s1, 2) ^ Integer.parseInt(s2, 2)).toBinaryString
            }
            
            def step(genString: String, infoString: String): String = {
                if infoString.length <= 10 then
                    infoString
                else
                    val divisor = genString.padTo(infoString.length, '0')
                    
                    val newInfoString = xor(infoString, divisor)
                    
                    step(genString, newInfoString)
            }
            
            val intermediateString = infoStringRaw + step(genString, infoString).reverse.padTo(10, '0').reverse
            
            assert(infoStringRaw.length == 5)
            assert(intermediateString.length == 15)
            
            xor(intermediateString, "101010000010010").reverse.padTo(15, '0').reverse
            
        }
        
        def placeInfoBits(qrArr: QRArrType, infoBits: String): QRArrType = {
            
            val infoMsg = infoBits.map({
                case '1' => QRModule.Black
                case '0' => QRModule.White
            }).toList
            
            val topLeftCoords = Map(
                (0, (8, 0)),
                (1, (8, 1)),
                (2, (8, 2)),
                (3, (8, 3)),
                (4, (8, 4)),
                (5, (8, 5)),
                (6, (8, 7)),
                (7, (8, 8)),
                (8, (7, 8)),
                (9, (5, 8)),
                (10, (4, 8)),
                (11, (3, 8)),
                (12, (2, 8)),
                (13, (1, 8)),
                (14, (0, 8))
            )
            
            val topRightCoords = (7 to 14).map(i => (i, (8, i + 18))).toMap
            
            val bottomLeftCoords = (0 to 6).map(i => (i, (32 - i, 8))).toMap
            
            infoMsg.zipWithIndex.map({(b, i) =>
                
                val tl = topLeftCoords(i)
                qrArr(tl(0))(tl(1)) = b
                
                if i <= 6 then
                    val bl = bottomLeftCoords(i)
                    qrArr(bl(0))(bl(1)) = b
                
                else
                    val tr = topRightCoords(i)
                    qrArr(tr(0))(tr(1)) = b
            })
            
            qrArr
        }
    
    }
    
    object Renderer {
        import QRArray.*
        
        def generate(qrArr: QRArrType, outFname: String = "QR.png"): Unit = {
            
            val arrWidth = qrArr.length
            
            val image = new BufferedImage(arrWidth, arrWidth, BufferedImage.TYPE_INT_RGB)
            
            for (i <- (0 until arrWidth);
                 j <- (0 until arrWidth)) {
                
                val pixel = qrArr(i)(j)
                
                val c = pixel match {
                    case _: WhiteColour => (255 * 65536) + (255 * 256) + 255
                    case _: BlackColour => 0
                }
                image.setRGB(i, j, c)
            }
            
            ImageIO.write(convertToBufferedImage(image.getScaledInstance(512, 512, Image.SCALE_REPLICATE))
                , "png", new File(outFname))
        }
        
        def convertToBufferedImage(img: Image) = {
            
            // Create a buffered image with transparency
            val bi = new BufferedImage(
                img.getWidth(null), img.getHeight(null),
                BufferedImage.TYPE_INT_ARGB);
            
            val graphics2D = bi.createGraphics();
            graphics2D.drawImage(img, 0, 0, null);
            graphics2D.dispose();
            
            bi
        }
    }
    
}