package lib.utils {
    
    import scala.annotation.tailrec
    
    object QRHelpers {
        extension (sc: StringContext) {
            
            def lpz(args: String*): String = {
                
                @tailrec def pad(s: String, n: Int): String = {
                    if s.length < n then {
                        pad("0" + s, n)
                    } else {
                        s
                    }
                }
                
                pad(args(0), Integer.parseInt(args(1)))
            }}
        
        def leftPadZeroBinaryString(s: String, max_length: Int): String = {
            lpz"${s}${max_length.toString}"
        }
    }
    
}

