package lib.qrdata {
    
    import lib.utils.QRHelpers.leftPadZeroBinaryString
    import scala.util.matching.Regex
    import scala.annotation.tailrec
    
    /*
    Version 4 Error Correction Levels and Details
     */
    enum ErrorCorrectionLevel(val nCodeWords: Int,
                              val numericCapacity: Int,
                              val alphaNumericCapacity: Int,
                              val byteCapacity: Int,
                              val nBlocks: Int,
                              val nECCodewords: Int,
                              val codewordsPerBlock: Int,
                              val generatorPolyExponents: List[Int],
                              val infoIDString: String
                             ) {
        case L extends ErrorCorrectionLevel(
            80,
            187,
            114,
            78,
            1,
            20,
            80,
            List(0,17,60,79,50,61,163,26,187,202,180,221,225,83,239,156,164,212,212,188,190),
            "01")
            
        case M extends ErrorCorrectionLevel(
            64,
            149,
            90,
            62,
            2,
            18,
            32,
            List(0,215,234,158,94,184,97,118,170,79,187,152,148,252,179,5,98,96,153),
            "00")
            
        case Q extends ErrorCorrectionLevel(
            48,
            111,
            67,
            46,
            2,
            26,
            29,
            List(0,173,125,158,2,103,182,118,17,145,201,111,28,165,53,161,21,245,142,13,102,48,227,153,145,218,70),
            "11")
            
        case H extends ErrorCorrectionLevel(
            36,
            82,
            50,
            34,
            4,
            16,
            9,
            List(0,120,104,107,109,102,161,76,3,91,191,147,169,182,194,225,120),
            "10")
            
    }
    
    /*
    QR objects
     */
    enum QR(val data: Option[String], val mode: String, val charCountNBits: Int) {
        case NumericQR(dataStr: Option[String]) extends QR(dataStr, "0001", 10)
        case AlphaNumericQR(dataStr: Option[String]) extends QR(dataStr, "0010", 9)
        case ByteQR(dataStr: Option[String]) extends QR(dataStr, "0100", 8)
        case InvalidQR extends QR(None, "0000", 0)
    }
    
    object QR {
        
        val numericPattern: Regex = "^([0-9]+)$".r
        val alphaNumericPattern: Regex = "^([A-Z0-9$%*+-./: ]+)$".r
        val bytePattern: Regex = "^([\\x00-\\x7F\\xA0-\\xFF]+)$".r
        
        def apply(s: String) = {
            s match {
                case numericPattern(data) => NumericQR(Some(data))
                case alphaNumericPattern(data) => AlphaNumericQR(Some(data))
                case bytePattern(data) => ByteQR(Some(data))
                case _ => InvalidQR
            }
        }
    }
    
    /*
    QR data encoders
     */
    def encodeQRData[A <: QR](qr: A) = {
        qr match {
            case _: QR.NumericQR => NumericQREncoder.encode(qr)
            case _: QR.AlphaNumericQR => AlphaNumericQREncoder.encode(qr)
            case _: QR.ByteQR => ByteQREncoder.encode(qr)
        }
    }
    
    object NumericQREncoder {
        
        def encode(qr: QR): String = {
            
            qr.data.map(s => s.sliding(3, 3).map(ss => get3DigitBinary(ss)).mkString).get
            
        }
        
        def get3DigitBinary(s: String) = {
            
            val strippedS = s.dropWhile(_.toString == "0")
            val nDigits = strippedS.length
            val binStr = strippedS.toInt.toBinaryString
            
            nDigits match {
                case 3 => leftPadZeroBinaryString(binStr, 10)
                case 2 => leftPadZeroBinaryString(binStr, 7)
                case 1 => leftPadZeroBinaryString(binStr, 4)
            }
        }
    }
    
    object AlphaNumericQREncoder {
        
        private val alphaNumericChars = ('0' to '9') ++ ('A' to 'Z') ++ Vector(' ', '$', '%', '*', '+', '-', '.', '/', ':')
        
        private val charMap: Map[Char, Int] = alphaNumericChars.zip(0 to 44).toMap
        
        def encode(qr: QR): String = {
            qr.data.map(s => s.sliding(2, 2).map(ss => get2CharBinaryString(ss)).mkString).get
        }
        
        def get2CharBinaryString(s: String) = {
            if s.length == 2 then {
                val idxArr = s.map(c => charMap(c))
                
                leftPadZeroBinaryString(((45 * idxArr(0)) + idxArr(1)).toBinaryString, 11)
            } else {
                
                leftPadZeroBinaryString(charMap(s.charAt(0)).toBinaryString, 6)
                
            }
        }
    }
    
    object ByteQREncoder {
        def encode(qr: QR): String = {
            qr.data.map(s => s.getBytes.map(s => leftPadZeroBinaryString(s.toInt.toBinaryString, 8)).mkString).get
        }
    }
    
    /*
    Functions
     */
    object QRBitStringFuncs {
        def getQRCharCountBitString(qr: QR): Option[String] = {
            qr.data.map(s => leftPadZeroBinaryString(s.length.toBinaryString, qr.charCountNBits))
        }
        
        def getErrorCorrectionCapacity(l: ErrorCorrectionLevel, qr: QR): (ErrorCorrectionLevel, Int) = {
            
            val nChars = qr.data.map(s => s.length).get
            
            qr match {
                case _: QR.NumericQR => (l, l.numericCapacity - nChars)
                case _: QR.AlphaNumericQR => (l, l.alphaNumericCapacity - nChars)
                case _: QR.ByteQR => (l, l.byteCapacity - nChars)
            }
            
        }
        
        def getErrorCorrectionLevel(qr: QR): Option[ErrorCorrectionLevel] = {
            
            val l = ErrorCorrectionLevel.values
                .map(l => getErrorCorrectionCapacity(l, qr))
                .filter((l, n) => n > 0)
                .reduceOption((L1, L2) => (L1, L2) match {
                    case (L1, L2) if L1(1) < L2(1) => L1
                    case (L1, L2) if L1(1) >= L2(1) => L2
                })
            
            l.map(_(0))
        }
        
        def getDataBitString(qr: QR): String = qr.mode + getQRCharCountBitString(qr).get + encodeQRData(qr)
        
        def addTerminalBits(dataBits: String, requiredBits: Int): Option[String] = {
            
            val delta = (requiredBits - dataBits.length)
            if delta >= 0 then Some(dataBits + "0" * (4 min delta))
            else None
            
        }
        
        def padToFullByte(dataBits: Option[String]): Option[String] = {
            dataBits match {
                case None => None
                case Some(s) => {
                    val l = s.length
                    val delta = 8 - (l % 8)
                    
                    Some(s + ("0" * delta))
                }
            }
        }
        
        def padToFullCapacity(dataBits: Option[String], requiredBits: Int): Option[String] = {
            
            val padBytes: List[String] = List("11101100", "00010001")
            
            val nPadBytes: Option[Int] = dataBits match {
                case None => None
                case Some(s) => Some((requiredBits - s.length) / 8)
            }
            
            @tailrec
            def pad(s: Option[String], n: Int, maxN: Int): Option[String] = {
                if n > 0 then pad(s.map(ss => ss + padBytes((maxN - n) % 2)), n - 1, maxN)
                else s
            }
            
            nPadBytes match {
                case None => None
                case Some(maxN) => pad(dataBits, maxN, maxN)
            }
        }
        
        def getFullBitString(qr: QR): Option[String] = {
            
            val l = getErrorCorrectionLevel(qr)
            val requiredBits = l.get.nCodeWords * 8
            val dataBitString = getDataBitString(qr)
            
            for (s1 <- addTerminalBits(dataBitString, requiredBits);
                 s2 <- padToFullByte(Some(s1));
                 s3 <- padToFullCapacity(Some(s2), requiredBits)) yield s3
        }
        
        def getFullDataCodeWords(qr:QR): Option[List[Int]] = {
            getFullBitString(qr).map(_.sliding(8, 8).map(Integer.parseInt(_, 2)).toList)
        }
    }
    
}
    
