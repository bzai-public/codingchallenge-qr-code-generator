package lib.errorCorrection {

    import scala.annotation.tailrec
    import lib.utils.QRHelpers.leftPadZeroBinaryString
    
    def generateIntExponentMap(): (Map[Int, Int], Map[Int, Int]) = {
        val intExpTuples = (1 to 255).foldLeft(List[(Int, Int)]((1, 0)))({
            case (h::t, i) => {
                
                if h(0) * 2 <= 255 then {
                    ((h(0) * 2), i) :: h :: t
                } else {
                    ((h(0) * 2) ^ 285, i) :: h :: t
                }
            }
        })
        
        val expIntTuples = intExpTuples.map((i, j) => (j, i))
        
        (expIntTuples.toMap, intExpTuples.toMap)
    }
    
    val (expIntMap, intExpMap) = generateIntExponentMap()
    
    def errorCodeWordStep(generatorAlphas: List[Int], messageInts: List[Int]): List[Int] = {
        
        val messageAlpha0 = if messageInts(0) != 0 then intExpMap(messageInts(0)) else 0
        
        val divisorInts = generatorAlphas.map(a => (a + messageAlpha0)%255)
            .map(a => expIntMap(a))
        
//        println(messageAlphas.map(n=>expIntMap(n.get)).zipAll(divisorInts, 0, 0))
  
        messageInts
            .zipAll(divisorInts, 0, 0)
            .map((intM, intG) => {
                (intM, intG) match {
                    case (0, intG) => intG
                    case (intM, 0) => intM
                    case (intM, intG) => intM ^ intG
                }
            })
            .drop(1)
        
    }
    
    def generateErrorCodewords(n: Int, messageInts: List[Int], generatorAlphas: List[Int]): List[Int] = {
        
        @tailrec
        def go(n: Int, acc: List[Int]): List[Int] = {
            
            if n <= 0 then {
                acc
            } else {
                go(n - 1, errorCodeWordStep(generatorAlphas, acc))
            }
        }
        
        go(n, messageInts)
    }
    
    def generateBlockCodewords(fullMessageInts: Option[List[Int]],
                               generatorAlphas: List[Int],
                               nDataCodewordsPerBlock: Int,
                               nBlocks: Int): Option[List[(List[Int], List[Int])]] = {

        fullMessageInts.map({
            _.sliding(nDataCodewordsPerBlock, nDataCodewordsPerBlock)
                .map(msg => {
                    (msg, generateErrorCodewords(msg.length, msg, generatorAlphas))
                })
                .toList
        })
    }
    
    def interleaveLists[A, S](state: S)(f: S => Option[(List[A], S)]): List[A] = {
        f(state) match {
            case Some((a, s)) => a ++ interleaveLists(s)(f)
            case None => List.empty
        }
    }
    
    def interleaveCodewords(ls: List[Int]*) = {
        
        interleaveLists(ls)({
            ll =>
                if ll.nonEmpty then {
                    val elems = ll.map(_.headOption).filter(_.nonEmpty).toList
                    
                    Some(elems, ll.filter(l => l.nonEmpty).map(_.tail).toList)
                } else {
                    None
                }
        }).map(_.get)
    }
    
    def getDataErrorCodewords(codewordsByBlock: List[(List[Int], List[Int])]): List[Int] = {
        val dataBlocks = interleaveCodewords(codewordsByBlock.map(_(0))*)
        val errorBlocks = interleaveCodewords(codewordsByBlock.map(_(1))*)
        
        dataBlocks ++ errorBlocks
        
    }
    
    def getDataErrorBinaryString(dataErrorCodewords:List[Int]): String = {
        dataErrorCodewords.map(s => leftPadZeroBinaryString(s.toBinaryString, 8)).mkString
    }
    
    def generateBinaryStringWithErrorCorrection(fullMessageInts: Option[List[Int]],
                                                generatorAlphas: List[Int],
                                                nDataCodewordsPerBlock: Int,
                                                nBlocks: Int): String = {
        
        val blockCodes = generateBlockCodewords(fullMessageInts, generatorAlphas, nDataCodewordsPerBlock, nBlocks).get
        
        val allCodewords = getDataErrorCodewords(blockCodes)
        
        // we are only working on version 4 QR codes. That's why we mechanically add 7 "0" bits at the end.
        // For other versions there are other additional number of bits to be added.
        // See https://www.thonky.com/qr-code-tutorial/structure-final-message
        getDataErrorBinaryString(allCodewords) + "0000000"
        
        
    }
}


