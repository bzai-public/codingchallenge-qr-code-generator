import org.scalatest.funsuite.AnyFunSuite
import lib.qrdata.{QR, encodeQRData}
import lib.qrdata.QRBitStringFuncs.*
import lib.errorCorrection.*
import lib.qrdata.ErrorCorrectionLevel.L
import lib.qrimage.*

class QREncoderSuite extends AnyFunSuite {
    
    test("Encoding AlphaNumeric QR data") {
        
        val s = "HELLO WORLD"
        val dat = QR(s)
        
        assert(encodeQRData(dat) == "0110000101101111000110100010111001011011100010011010100001101")
        
    }
    
    test("Encoding Numeric QR data") {
        
        val s = "8675309"
        val dat = QR(s)
        
        assert(encodeQRData(dat) == "110110001110000100101001")
        
    }
    
    test("Encoding Byte QR data") {
        
        val s = "Hello, world!"
        val dat = QR(s)
        
        assert(encodeQRData(dat) == "01001000011001010110110001101100011011110010110000100000011101110110111101110010011011000110010000100001")
        
    }
}

class ErrorCorrectionSuite extends AnyFunSuite {
    test("ReedSolomon error correction codewords") {
        val msg = List(32, 91, 11, 120, 209, 114, 220, 77, 67, 64, 236, 17, 236, 17, 236, 17)
        val gen = List(0, 251, 67, 46, 61, 118, 70, 64, 94, 32, 45)
        
        val code = generateErrorCodewords(
            16,
            msg,
            gen)
//            .map(n => expIntMap(n))
        
        assert(code == List(196,35,39,119,235,215,231,226,93,23))
    }
    
    test("Error codeword generation on message") {
        val msg = List(67, 85, 70, 134, 87, 38, 85, 194, 119, 50, 6, 18, 6, 103, 38)
        val gen = List(0, 215, 234, 158, 94, 184, 97, 118, 170, 79, 187, 152, 148, 252, 179, 5, 98, 96, 153)
        
        val (_, eccodes) = generateBlockCodewords(Some(msg), gen, msg.length, 1).get(0)
        
        assert(eccodes == List(213, 199, 11, 45, 115, 247, 241, 223, 229, 248, 154, 117, 154, 111, 86, 161, 111, 39))
    }
}

class MaskPenaltySuite extends AnyFunSuite {
    
    test("Blocks same colour penalty: Same coloured blocks found") {
        val p = QRMask.MaskPenalties.blocksSameColor(Array(Array[QRModule](QRModule.White, QRModule.White), Array[QRModule](QRModule.White, QRModule.White)))
        
        assert(p == 3)
    }
    
    test("Blocks same colour penalty: No same coloured blocks found") {
        val p = QRMask.MaskPenalties.blocksSameColor(Array(Array[QRModule](QRModule.Black, QRModule.White), Array[QRModule](QRModule.White, QRModule.White)))
        
        assert(p == 0)
    }
    
    test("Match bit colour function: Different colour") {
        assert(!QRMask.MaskPenalties.matchBitColor(QRModule.White, QRModule.Black))
    }
    
    test("Match bit colour function: Same colour") {
        assert(QRMask.MaskPenalties.matchBitColor(QRModule.Black, QRModule.Black))
    }
}

class QRInfoSuite extends AnyFunSuite {
    test("QR Info String: Error Correction = L, Mask type = 4") {
        assert(QRInfo.calculateInfoBits(L, 4) == "110011000101111")
    }
}