# Coding Challenge - QR Code Generator

## Overview
This is my implementation of the QR Code Generator as laid out in [codingchallenges.fyi](https://codingchallenges.fyi/challenges/challenge-qr-generator).

The language chose is **Scala**.

## To use
1. In [`main.scala`](src/main/scala/main.scala), replace `s` with the string you want to encode
2. Run the program
3. There should be a QR code PNG image generated in the root of this folder.

## Thoughts \& Reflections
- It was really interesting to learn about the underlying mechanisms in the QR Code
- I now have a newfound respect for the amount of knowledge and engineering that goes into something which most of us take for granted like the QR Code
- I had a lot of fun learning about Reed-Soloman error correction (and actually trying to implement it in the QR code generator)
- The entire project was a lot more complicated that I thought it would be. There are quite a few *special cases* that needed to be dealt with in the generation process, such as making sure that the data bits were placed to the *left* of the *vertical* alignment patterns when you encounter them.
- Tried to follow as closely as possible the principles of functional programming. For example, no side effects, map/fold/reduce and higher-order functions.

